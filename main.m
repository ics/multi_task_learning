clear all
close all
clc

warning('off')

% in this simulation the approximate model has the same form as the true
% model, i.e. a linear combination of trigonometric basis functions. The
% following routines are optimizing frequencies and other hyperparameters
% of the model

%% set parameters
dosave = 1;
doplot = 1;

E_imp = [4]; % chosen number of frequencies
E_true = [4]; % true number of frequencies
D = [1]; % dimension of the frequency
M = [3]; % number of tasks
N = [50]; % data per task


for a=1:1
    rng(a);
    seed = rng;
    for b=1:length(E_imp)
        for c=b:length(E_true) %we only consider the case in which E_true is greater or equal than E_imp
            for d=1:length(D)
                for e=1:length(M)
                    for f=1:length(N)
                        w_true = zeros(E_true(c)/2,D(d));
                        rng(seed)
                        for g=1:D(d)
                            w_true(:,g) = sort(2*rand(E_true(c)/2,1));
                        end
                        
                        rng(seed)
                        alfa_true = round(10*rand(E_true(c),M(e))-5*ones(E_true(c),M(e)),1);
                        a_true = alfa_true(1:E_true(c)/2,:);
                        b_true = alfa_true(E_true(c)/2 + 1:end,:);
                       
               
                        params = struct( 'M', M(e), ...  number of tasks
                                         'N', N(f), ... number of input locations per each task
                                         'w', w_true, ...  sinusoidal frquency (E/2 x D)
                                         'a', a_true, ... linear combinator (E/2 x M)
                                         'b', b_true, ... linear combinator (E/2 x M)
                                         'alfa', alfa_true, ... complete vector of combinators (a1 b1 a2 b2...)
                                         'E', E_imp(b) , ... number of sinusoidals (E/2 basis functions [sin ; cos])
                                         'D', D(d), ... number of input location features
                                         't_minmax', [0,2] ); %span of input locations


                        [params,input_locations,y] = data_generation(params,seed);
                        
                         params = GP(params,input_locations,y,doplot);
                         params = mGP(params,input_locations,y,doplot);
                         
                         params = SGP(params,input_locations,y,doplot);
                         
                         params = KLgp(params,input_locations,y,doplot);
                         params = iKLgp(params,input_locations,y,doplot);
                         params = RLS(params,input_locations,y,doplot);
                         params = KLdata(params,input_locations,y,doplot); % same routine used for RA-L paper using data generated from simulations and experiments
                         
                         params = MCMC(params,input_locations,y,doplot);

                        if dosave
                            name_file = strcat('Simulations/params_',num2str(a),'_',num2str(E_imp(b)),'_',num2str(E_true(c)),'_',num2str(D(d)),'_',num2str(M(e)),'_',num2str(N(f))); 
                            save(name_file,'params');
                        end
                    end
                end
            end
        end
    end
end

