function [alfas,lambdas,sigmas] = gibbsSingleTaskFF(N,E,X,Y,omegas_est)

% GIBBS SAMPLER FOR REFINEMENT, i.e. single task, fixed frequency

% MODEL: y_t = sum_{i=1}^E/2 \alpha_i sin(2\pi\omega_i x_t)
% + \alpha_{i+E\2} cos(2\pi\omega_i x_t)
% NO INPUT LOCATIONS PRUNING

% @input:
%         N = #inputlocations
%         E = #frequencies
%         X,Y = input,output data
%         omegas_est = estimated frequencies (from multitask step)
% @output: cell arrays containing all the evolution of the variables 
%          within the Gibbs sampler 

mcmc = 500; % MCMC iterations
alfas = cell(mcmc,1); % each cell contains an E-dimensional column vector 
lambdas = cell(mcmc,1); % each cell contains an E-dimensional column vector
sigmas = cell(mcmc,1); % each cell contains a scalar
eps_omega_1 = 1; % first prior coefficient for Gamma prior on \omega
eps_omega_2 = 1; % second prior coefficient for Gamma prior on \omega

Phi = generatePhi(omegas_est,X);

for g = 1:mcmc
    % sample alpha
    if g ==1
        alfas{g} = (Phi'*Phi)\((Phi')*Y);
    else
        lambdas_old = lambdas{g-1};
        Sigma_alfa_old =diag(lambdas_old);
        sigma2_old = sigmas{g-1};
        mu_hat_alfa = Sigma_alfa_old*(Phi')*((Phi*Sigma_alfa_old*(Phi')+sigma2_old*eye(N))\Y);
        Sigma_hat_alfa = inv(inv(Sigma_alfa_old) + Phi'*Phi/sigma2_old);
        Sigma_hat_alfa = (Sigma_hat_alfa+Sigma_hat_alfa')/2;
        square_Sigma_hat_alfa = zeros(E,E);
        try
            square_Sigma_hat_alfa = chol(Sigma_hat_alfa);
        catch
            Sigma_hat_alfa = Sigma_hat_alfa + eye(E);
            square_Sigma_hat_alfa = chol(Sigma_hat_alfa); % introduce a perturbation in the covariance
        end
        alfas{g} = mu_hat_alfa + square_Sigma_hat_alfa*randn(E,1);
        
    end
    
    % sample lambdas
    lambdas_new = zeros(E,1);
    alfa = alfas{g};
    for i=1:E
        lambdas_new(i) = inv(gamrnd(0.5, inv(0.5*alfa(i)^2)));
    end
    lambdas{g} = lambdas_new;
    
    
    % sample sigma2
    alfa = alfas{g};
    sigmas{g} = inv(gamrnd(0.5*N, inv(0.5*norm(Y-Phi*alfa)^2)));
   
end
