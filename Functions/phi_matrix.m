function Phi = phi_matrix(X,omega,symb)
%PHI_MATRIX 
%   Basis function matrix composed of sines and cosines centered at
%   spectral frequencies omega, for all input locations X

% X is and NxD matrix
% omega is an E/2 x D matrix where E are the number of basis functions

import casadi.*

if symb
    Phi =  SX.sym('Phi',size(X,1),size(omega,1)*2); 
else
    Phi =  zeros(size(X,1),size(omega,1)*2);
end
for i=1:size(X,1)
    for j=1:2:size(omega,1)*2
        
        Phi(i,[j,j+1]) = phi_single(X(i,:)',omega((j+1)/2,:)');
        
    end
end

end

