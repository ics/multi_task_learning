function Phi = generatePhi(omegas,X)

[N,d] = size(X);
[halfE,d] = size(omegas);
E = 2*halfE;
Phi = zeros(N,E);

for t=1:N
    for i=1:halfE
        Phi(t,i) = sin(2*pi*omegas(i,:)*(X(t,:)'));
        Phi(t,i+halfE) = cos(2*pi*omegas(i,:)*(X(t,:)'));
    end
end

% i-th row of omegas stores the d-dimensional vector \omega_i^{\top}
% t-th row of X stores the d-dimensional vector x_t^{\top}



end
