function [params,input_locations,y] = data_generation(params,seed)

rng(seed);


input_locations = (params.t_minmax(2) - params.t_minmax(1))*rand(params.N,params.D) + params.t_minmax(1)*ones(params.N,params.D); % (N x D) 
noise_vars_tmp = 0.5*rand(params.M,1);
noise_vars_tmp(noise_vars_tmp < 0.01) = 0.01;

params.noise_var = noise_vars_tmp;

y = cell(params.M,1);

f_true = zeros(size(input_locations,1),params.M);
for m=1:params.M
    rng(seed);
    errors = sqrt(params.noise_var(m))*randn(params.N,1);
    Phi = generatePhi(params.w,input_locations);
    y{m,1} = Phi*params.alfa(:,m) + errors;
end



%{
noise_std = zeros(params.M,1);
y = cell(params.M,1);
for i=1:params.M
    noise_std(i) = abs(0.2*randn);
    if noise_std(i) < 0.1
        noise_std(i) = 0.1;
    end
    
    noise_vector = noise_std(i)*randn(params.N,1);
    f = generate_sinusoidal_data(params.w,params.a(:,i),params.b(:,i),input_locations);
    y{i} = f + noise_vector;
    %name_file = strcat('Data/y_',num2str(i)); %save signal in mat file as Data/y_i denoting noisy task data
    %save(name_file,'y')
end

%}


%save('Data/input_locations', 'input_locations');


%params.noise_var =noise_std.^2;  %noise variance for each task

%save('Data/params','params');

end