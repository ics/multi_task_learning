function [lambdas_est, sigmas2_est, omegas_est, Phi_est] = optiMargLikGibbs(M,N,E,X,Y,G)

[N,d] = size(X);    
NegLogMargLiks = zeros(G,1);

% initialize optimal values
lambdas_est = zeros(E,M);
sigmas2_est = zeros(M,1);
omegas_est = zeros(E/2,d);
Phi_est = zeros(N,E);
opt_index = 0;

for g=1:G
    
    % store the final parameters of the Gibbs sampler
    
    [alfas,lambdas,sigmas,omegas,Phis] = gibbsMultiTask1A(M,N,E,X,Y);
    lambdas_tmp = cell2mat(lambdas(end,:));
    sigmas2_tmp = cell2mat(sigmas(end,:));
    omegas_tmp = omegas{end};
    Phi_tmp = Phis{end};
    
    % evaluate the negative log marginal likelihood (sum of the -log MarLik of each task)
    
    NegLogMargLik_tmp = 0;
    for m=1:M
        Sigma_alfa_m = diag(lambdas_tmp(:,m));
        Y_m = Y(:,m);
        sigma2_m = sigmas2_tmp(m);
        NegLogMargLik_tmp = NegLogMargLik_tmp + log(det(Phi_tmp*Sigma_alfa_m*Phi_tmp' + sigma2_m*eye(N))) + Y_m'*((Phi_tmp*Sigma_alfa_m*Phi_tmp' + sigma2_m*eye(N))\Y_m);
    end
    NegLogMargLiks(g) = NegLogMargLik_tmp;
    
    if g == 1
        lambdas_est = lambdas_tmp;
        sigmas2_est = sigmas2_tmp;
        omegas_est = omegas_tmp;
        Phi_est = Phi_tmp;
        opt_index = 1;
        
    else
        if NegLogMargLiks(g) < NegLogMargLiks(opt_index)
            lambdas_est = lambdas_tmp;
            sigmas2_est = sigmas2_tmp;
            omegas_est = omegas_tmp;
            Phi_est = Phi_tmp;
            opt_index = g;
        end
    end
end


