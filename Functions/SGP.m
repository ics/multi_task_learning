function params = ssgp_opt(params,input_locations,y,doplot)

import casadi.*

for j=1:params.M
    signal_var = SX.sym('signal_var',1);
    noise_var = SX.sym('noise_var',1);
    omega = SX.sym('omega',params.E/2,params.D); %spectral frequency
    Phi = phi_matrix(input_locations,omega,1);
    A = Phi'*Phi + (params.E/2)*noise_var/signal_var*eye(params.E)*1.0;
    R = chol(A);


%    Lambda = params.eta_sol(j,:);
%    sigma = diag(1./Lambda/4/pi/pi);

% draw E_imp/2 samples
%    omega_r = zeros(params.E/2,params.D);
%     for i = 1:params.E/2
%         omega_r(i,:) = mvnrnd(zeros(1,params.D),sigma);
%     end
    cost = - y{j}'*Phi*(mldivide( A,Phi'*y{j} ) )/(2*noise_var.^2) + y{j}'*y{j}/(2*noise_var.^2) + 0.5*trace(log(R)) - ...
        (params.E/2)*log(noise_var.^2/signal_var.^2*params.E/2) + params.N/2*log(2*pi*noise_var.^2);

    sol = [signal_var;noise_var;omega(:)];

    nlp = struct('x',sol, 'f',cost);
    opt = struct('print_time',false,'ipopt',struct('print_level',0,'file_print_level',0,'max_iter',100));
    S = nlpsol('S', 'ipopt', nlp,opt);
    
    x0 = [0.5,1,1.5,2];
    cost_opt = +inf;
    for p=1:length(x0)

        r = S('x0',x0(p),'lbx',-inf,'ubx',inf);
        cost_opt_tmp = full(r.f);
        x_opt = full(r.x);
        %disp(x_opt)

        if cost_opt_tmp < cost_opt
            signal_var_sol{j} = x_opt(1).^2;
            noise_var_sol{j} = x_opt(2).^2;
            omega_sol_opt{j} = reshape(x_opt(3:end),params.E/2,params.D);
            gamma_var_sol{j} = 1/(x_opt(1).^2);
            cost_opt = cost_opt_tmp;
        end
    end
    %disp(x_opt)
    %omega_sol{j} = 2*pi*omega_r;
end


%%
x_test = [];
for i=1:params.D
    x_test = [x_test,linspace(params.t_minmax(1),params.t_minmax(2),1000)'];
end
f_test = zeros(size(x_test,1),params.M);
for i=1:params.M
    A = phi_matrix(input_locations,omega_sol_opt{j},0)'*phi_matrix(input_locations,omega_sol_opt{j},0) + (params.E/2)*noise_var_sol{i}/signal_var_sol{i}*eye(params.E)*1.0;
    mean_alpha = ( A\( phi_matrix(input_locations,omega_sol_opt{j},0)'*y{i} ) );
    f_test(:,i) = phi_matrix(x_test,omega_sol_opt{j},0)*mean_alpha;
end



x_true = [];
for i=1:params.D
    x_true = [x_true,linspace(params.t_minmax(1),params.t_minmax(2),1000)'];
end % (N x D)
f_true = zeros(size(x_true,1),params.M);
for j =1:params.M
    for i=1:size(params.w,1)
        f_true(:,j) = f_true(:,j) + params.a(i,j)*sin(2*pi*x_true*params.w(i,:)') + params.b(i,j)*cos(2*pi*x_true*params.w(i,:)');
    end
end

RMSE_ssgp_opt = zeros(params.M,1);
for i=1:params.M
    RMSE_ssgp_opt(i) = sqrt(mean((f_true(:,i) - f_test(:,i)).^2));
end


if doplot
    for i=1:params.M
        figure;
        hold on
        plot(f_test(:,i));
        %plot(y{i},'*');
        plot(f_true(:,i));
        title(['SSGP optimized task= ',num2str(i)])
    end
end
%%
% params.eta_sol = eta_sol;
params.sigma_sol_ssgp_opt = noise_var_sol;
params.gamma_sol_ssgp_opt = gamma_var_sol;
params.RMSE_ssgp_opt = RMSE_ssgp_opt;
params.omega_sol_ssgp_opt = omega_sol_opt;


%save('Data/params','params')
end
