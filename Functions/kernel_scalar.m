function k = kernel_scalar(x,y,eta)
%KERNEL_SCALAR 
%   Defines a single component of the kernel matrix between two input
%   locations x and y, with ARD lengthscales eta

%   x is a Dx1 vector
%   y is a Dx1 vector
%   lambda is a Dx1 vector (lengthscales for each dimension)

tmp = 0;
for i=1:length(x)
    
    tmp = tmp + (x(i) - y(i))^2/eta(i) ;
    
end
k = exp(-tmp);

end

