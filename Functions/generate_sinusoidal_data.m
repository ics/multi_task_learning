function f = generate_sinusoidal_data(w,a,b,input_locations)
%GENERATE_SINUSOIDAL_DATA: generate sinusoidal -> sqrt(a^2 + b^2)cos(wt - arctan(a/b)) = a*sin(wt) + b*cos(w*t)

%   input_locations: (N x D)
%   w: is the chosen frequency (E/2 x D)
%   a: is the first linear combinator (E/2 x 1)
%   b: is the second linear combinator (E/2 x 1)

f=0;
for i=1:size(w,1)
    f = f + a(i)*sin(input_locations*w(i,:)') + b(i)*cos(input_locations*w(i,:)');
end

end

