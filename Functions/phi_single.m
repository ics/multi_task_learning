function phi = phi_single(x,omega)
%PHI_SINGLE 
%   this function defines a single component of the basis function matrix 
%   centered at input location x 

% x is a Dx1 vector
% omega is a Dx1 vector

phi = [sin(2*pi*omega'*x) , cos(2*pi*omega'*x) ]; %2x1 basis function

end

