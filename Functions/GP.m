function params = GP(params,input_locations,y,doplot)



eta_sol = zeros(params.M,params.D);
sigma_sol = zeros(params.M,1);
gamma_sol = zeros(params.M,1);
for i=1:params.M

meanfunc = [];                    % empty: don't use a mean function
covfunc = @covSEard;              % Squared Exponental covariance function
likfunc = @likGauss;              % Gaussian likelihood
hyp = struct('mean', [], 'cov', -1*ones(1,params.D+1), 'lik', -1);
hyp_opt = minimize(hyp, @gp, -100, @infGaussLik, meanfunc, covfunc, likfunc, input_locations, y{i});

sigma_sol(i) = exp(2*hyp_opt.lik);
gamma_sol(i) = 1/exp(2*hyp_opt.cov(end));
eta_sol(i,:) = exp(2*hyp_opt.cov(1:end-1));

end

%%

x_test = [];
for i=1:params.D
    x_test = [x_test,linspace(params.t_minmax(1),params.t_minmax(2),1000)'];
end
f_test = zeros(size(x_test,1),params.M);
for i=1:params.M
    f_test(:,i) = (kernel_matrix(x_test,eta_sol(i,:)',0,input_locations)/gamma_sol(i))*pinv(kernel_matrix(input_locations,eta_sol(i,:)',0)/gamma_sol(i) + sigma_sol(i)*eye(params.N))*y{i};
end
%mean5 = (kernel_matrix(x_test,eta_sol,0,input_locations)/gamma_sol)*pinv(kernel_matrix(input_locations,eta_sol,0)/gamma_sol + sigma_sol*eye(params.N))*y{5};


x_true = [];
for i=1:params.D
    x_true = [x_true,linspace(params.t_minmax(1),params.t_minmax(2),1000)'];
end % (N x D)
f_true = zeros(size(x_true,1),params.M);
for j =1:params.M
    for i=1:size(params.w,1)
        f_true(:,j) = f_true(:,j) + params.a(i,j)*sin(2*pi*x_true*params.w(i,:)') + params.b(i,j)*cos(2*pi*x_true*params.w(i,:)');
    end
end

RMSE = zeros(params.M,1);
for i=1:params.M
    RMSE(i) = sqrt(mean((f_true(:,i) - f_test(:,i)).^2));
end


if doplot
    for i=1:params.M
        figure;
        hold on
        plot(f_test(:,i));
        %plot(y{i},'*');
        plot(f_true(:,i));
        title(['Full GP task= ',num2str(i)])
    end
end

%%
params.eta_sol = eta_sol;
params.sigma_sol = sigma_sol;
params.gamma_sol = gamma_sol;
params.RMSE = RMSE;

%save('Data/params','params')
end
