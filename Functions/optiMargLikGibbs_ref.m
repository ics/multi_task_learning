function [lambda_est, sigma2_est, alfa_est] = optiMargLikGibbs_ref(N,E,X,Y,G,omegas_est)

    
NegLogMargLiks = zeros(G,1);

% initialize optimal values
lambda_est = zeros(E,1);
sigma2_est = 0;
Phi = generatePhi(omegas_est,X);
opt_index = 0;

for g=1:G
    % store the final parameters of the Gibbs sampler
    
    [alfas,lambdas,sigmas] = gibbsSingleTask1A(N,E,X,Y,omegas_est);
    lambda_tmp = lambdas{end};
    sigma2_tmp = sigmas{end};
    
    % evaluate the negative log marginal likelihood (sum of the -log MarLik of each task)
    
    NegLogMargLik_tmp = 0;
    Sigma_alfa = diag(lambda_tmp);
    NegLogMargLiks(g) = NegLogMargLik_tmp + log(det(Phi*Sigma_alfa*Phi' + sigma2_tmp*eye(N))) + Y'*((Phi*Sigma_alfa*Phi' + sigma2_tmp*eye(N))\Y);    
    
    if g == 1
        lambda_est = lambda_tmp;
        sigma2_est = sigma2_tmp;
        opt_index = 1;
        
    else
        if NegLogMargLiks(g) < NegLogMargLiks(opt_index)
            lambda_est = lambda_tmp;
            sigma2_est = sigma2_tmp;
            opt_index = g;
        end
    end
end

Sigma_alfa_est = diag(lambda_est);
alfa_est = Sigma_alfa_est*(Phi')*((Phi*Sigma_alfa_est*(Phi')+sigma2_est*eye(N))\Y);

