function [alfas,lambdas,sigmas,omegas,Phis] = gibbsMultiTask1A(M,N,E,X,Y)

% MULTIDIMENSIONAL CASE!

% MODEL: for each task, y_t = sum_{i=1}^E/2 \alpha_i sin(2\pi\omega_i x_t)
% + \alpha_{i+E\2} cos(2\pi\omega_i x_t)
% NO INPUT LOCATIONS PRUNING

% @input: M = #tasks
%         N = #inputlocations
%         E = #frequencies
%         X,Y = input,output data
% @output: cell arrays containing all the evolution of the variables 
%          within the Gibbs sampler 
[N,d] = size(X);
mcmc = 500; % MCMC iterations
alfas = cell(mcmc,M); % each cell contains an E-dimensional column vector 
lambdas = cell(mcmc,M); % each cell contains an E-dimensional column vector
sigmas = cell(mcmc,M); % each cell contains a scalar
omegas = cell(mcmc,1); % each cell contains a scalar
Phis = cell(mcmc,1); % each cell contains a NxE matrix
eps_omega_1 = 1; % first prior coefficient for Gamma prior on \omega
eps_omega_2 = 1; % second prior coefficient for Gamma prior on \omega

for g = 1:mcmc
    % sample omega and alpha
    if g ==1
        % sample random omega (inizialization) 
        omegas_tmp = zeros(E/2,d);
        for j = 1:d
            for i=1:E/2
                if i==1
                    omegas_tmp(i,j) = gamrnd(eps_omega_1,1/eps_omega_2);
                else
                    omegas_tmp(i,j) = omegas_tmp(i-1,j)+ gamrnd(eps_omega_1,1/eps_omega_2);
                end
            end
        end
        omegas{g} = omegas_tmp;
        % build regressors matrix
        Phi = generatePhi(omegas{g},X);
        Phis{g} = Phi;
        % draw random coefficients (initialization)
        for m = 1:M
            alfas{g,m} = (Phi'*Phi)\((Phi')*Y(:,m));
        end
        
    else
        % Metropolis-in-Gibbs for omega
        omegas_new = zeros(E/2,d);
        for j=1:d
            for i=1:E/2
                if i == 1
                    omegas_new(i,j) = gamrnd(eps_omega_1,1/eps_omega_2);
                else
                    omegas_new(i,j) = omegas_new(i-1,j)+ gamrnd(eps_omega_1,1/eps_omega_2);
                end
            end
        end
        
        omegas_old = omegas{g-1};
        Phi_old = Phis{g-1};
        sigmas2_old = cell2mat(sigmas(g-1,:));
        alfa_old = cell2mat(alfas(g-1,:));
        Phi_new = generatePhi(omegas_new,X);
        
        tmp_lik_old = 0;
        tmp_lik_new = 0;
        for m = 1:M
            tmp_lik_old = tmp_lik_old + norm(Y(:,m) - Phi_old*alfa_old(:,m))^2/sigmas2_old(m);
            tmp_lik_new = tmp_lik_new + norm(Y(:,m) - Phi_new*alfa_old(:,m))^2/sigmas2_old(m);
        end
     
        omegas_diff_old = zeros(E/2,d);
        omegas_diff_new = zeros(E/2,d);
        for j=1:d
            for i = 1:E/2
                if i ==1
                    omegas_diff_old(i,j) = omegas_old(i,j);
                    omegas_diff_new(i,j) = omegas_new(i,j);
                else
                    % operate column-wise
                    omegas_diff_old(i,j) = omegas_old(i,j)-omegas_old(i-1,j);
                    omegas_diff_new(i,j) = omegas_new(i,j)-omegas_new(i-1,j);
                end
            end
        end
        
        mat_col_prod_old = zeros(d,1);
        mat_col_prod_new = zeros(d,1);
        for j = 1:d
            omegas_diff_old_j = omegas_diff_old(:,j);
            omegas_diff_new_j = omegas_diff_new(:,j);
            mat_col_prod_old(j) = prod(exp(-omegas_diff_old_j*eps_omega_2).*(omegas_diff_old_j.^(eps_omega_1-1)));
            mat_col_prod_new(j) = prod(exp(-omegas_diff_new_j*eps_omega_2).*(omegas_diff_new_j.^(eps_omega_1-1)));
        end
       
        lik_old = exp(-tmp_lik_old)*prod(mat_col_prod_old);
        lik_new = exp(-tmp_lik_new)*prod(mat_col_prod_new);
        
        accept_omega = min([1; lik_new/lik_old]);
        u = rand(1);
        if u <= accept_omega
            omegas{g} = omegas_new;
        else
            omegas{g} = omegas_old;
        end
        Phis{g} = generatePhi(omegas{g},X);
        
        % update alfa
        for m=1:M
            lambdas_old = lambdas{g-1,m};
            Sigma_alfa_old = diag(lambdas_old);
            Phi = Phis{g};
            sigma2_old = sigmas{g-1,m};
            mu_hat_alfa = Sigma_alfa_old*(Phi')*((Phi*Sigma_alfa_old*(Phi')+sigma2_old*eye(N))\Y(:,m));
            Sigma_hat_alfa = inv(inv(Sigma_alfa_old) + Phi'*Phi/sigma2_old);
            Sigma_hat_alfa = (Sigma_hat_alfa+Sigma_hat_alfa')/2;
            square_Sigma_hat_alfa = zeros(E,E);
            try
                square_Sigma_hat_alfa = chol(Sigma_hat_alfa);
            catch
                % introduce a perturbation in the covariance
                Sigma_hat_alfa = Sigma_hat_alfa + eye(E);
                square_Sigma_hat_alfa = chol(Sigma_hat_alfa);
            end
            alfas{g,m} = mu_hat_alfa + square_Sigma_hat_alfa*randn(E,1);
        end
    end
    
    % sample lambdas
    for m = 1:M
        lambdas_new = zeros(E,1);
        alfa = alfas{g,m};
        for i=1:E
            lambdas_new(i) = inv(gamrnd(0.5, inv(0.5*alfa(i)^2)));
        end
        lambdas{g,m} = lambdas_new;
    end
    
    % sample sigma2
    for m = 1:M
        Phi = Phis{g};
        alfa = alfas{g,m};
        sigmas{g,m} = inv(gamrnd(0.5*N, inv(0.5*norm(Y(:,m) - Phi*alfa)^2)));
    end
    
end
