function new_idx = split_idx(idx)
%SPLIT_IDX Summary of this function goes here
%   Detailed explanation goes here

new_idx = zeros(2*length(idx),1);
for i=1:length(idx)
    new_idx([2*i-1,2*i]) = [idx(i)*2-1,idx(i)*2];
end

end

