function params = RLS(params,input_locations,y,doplot)


import casadi.*

%% Build cost function

% y = cell(1,params.M);
% for i=1:params.M
%     name_file = strcat('Data/y_',num2str(i)); 
%     y{i} = struct2array(load(name_file));
% end
% 
% load('Data/input_locations.mat');

%optimization variables intra-task

sigma = cell(1,params.M); %noise variance for each task data set
for i=1:params.M
    sigma{i} = SX.sym('sigma',1);
end

%optimization variables inter-task

lambda = SX.sym('lambda',params.E,1); %prior variance for sinusoidal linear parameters
omega = SX.sym('omega',params.E/2,params.D); %spectral frequency

%% cost function definition


cost = 0;
for i=1:params.M
   
   Phi = phi_matrix(input_locations,omega,1);
   
   inv_post_var_alpha = (inv(diag(lambda).^2) + Phi'*Phi/sigma{i}.^2);
   
   post_mean_alpha = mldivide(inv_post_var_alpha, (Phi'*y{i})/sigma{i}.^2 );
   
   q_mean_m = Phi*post_mean_alpha;
   
   q_var_m =  Phi*mldivide(inv_post_var_alpha, Phi');
   
   K = kernel_matrix(input_locations,params.eta_sol(i,:),0);
     
   p_var_m = K/params.gamma_sol(i) - K*mldivide((K + params.gamma_sol(i)*params.sigma_sol(i)*eye(params.N)),K)/params.gamma_sol(i);
         
   p_mean_m = K*mldivide((K + params.gamma_sol(i)*params.sigma_sol(i)*eye(params.N)),y{i});
         
   cost_m = norm( p_mean_m - q_mean_m) + 0.2*norm_fro( p_var_m - q_var_m );
    
   cost = cost + cost_m ; 
    
end
cost = cost/params.M;

% optimization via IPOPT

%optimization variables intra-task

sol = [];
for i=1:params.M
    sol = [sol;sigma{i}];   
end
%for i=1:M
%    sol = [sol;gamma{i}];   
%end
%for i=1:M
%    sol = [sol;eta{i}];   
%end

%optimization variables inter-task
sol = [sol;lambda;omega(:)];

nlp = struct('x',sol, 'f',cost);
opt = struct('print_time',false,'ipopt',struct('print_level',0,'file_print_level',0,'max_iter',100));
S = nlpsol('S', 'ipopt', nlp,opt);

x0 = [0.5,1,1.5,2];
cost_opt = +inf;
for p=1:length(x0)

    r = S('x0',x0(p),'lbx',-inf,'ubx',inf);
    cost_opt_tmp = full(r.f);
    x_opt = full(r.x);
    %disp(x_opt)
    
    if cost_opt_tmp < cost_opt
        omega_sol_meta_LS = reshape(x_opt(params.M +params.E+1:end),params.E/2,params.D);  
        lambda_sol_meta_LS = x_opt(params.M+1:params.M+params.E).^2;
        sigma_sol_meta_LS = x_opt(1:params.M).^2;
        cost_opt = cost_opt_tmp;
    end
end

%%

x_true = [];
for i=1:params.D
x_true = [x_true,linspace(params.t_minmax(1),params.t_minmax(2),1000)'];
end % (N x D)
f_true = zeros(size(x_true,1),params.M);
for j =1:params.M
for i=1:size(params.w,1)
    f_true(:,j) = f_true(:,j) + params.a(i,j)*sin(2*pi*x_true*params.w(i,:)') + params.b(i,j)*cos(2*pi*x_true*params.w(i,:)');
end
end

x_test = [];
for i=1:params.D
x_test = [x_test,linspace(params.t_minmax(1),params.t_minmax(2),1000)'];
end
f_test = zeros(size(x_test,1),params.M);
a_hat_LS = zeros(params.E/2,params.M);
b_hat_LS = zeros(params.E/2,params.M);
for i=1:params.M
mean_alpha = ((inv(diag(lambda_sol_meta_LS)) + phi_matrix(input_locations,omega_sol_meta_LS,0)'*phi_matrix(input_locations,omega_sol_meta_LS,0)/sigma_sol_meta_LS(i))\(phi_matrix(input_locations,omega_sol_meta_LS,0)'*y{i}))/sigma_sol_meta_LS(i);
f_test(:,i) = phi_matrix(x_test,omega_sol_meta_LS,0)*mean_alpha;
a_hat_LS(:,i) = mean_alpha(1:2:end);
b_hat_LS(:,i) = mean_alpha(2:2:end);
end

RMSE_meta_LS = zeros(params.M,1);
for i=1:params.M
RMSE_meta_LS(i) = sqrt(mean((f_true(:,i) - f_test(:,i)).^2));
end

if doplot
    for i=1:params.M
        figure;
       hold on
        plot(f_test(:,i));
        %plot(y{i},'*');
        plot(f_true(:,i));
        title(['meta LS posterior prediction task= ',num2str(i)])
    end
end

%%
params.omega_sol_meta_LS = omega_sol_meta_LS;
params.lambda_sol_meta_LS = lambda_sol_meta_LS;
params.sigma_sol_meta_LS = sigma_sol_meta_LS;
params.RMSE_meta_LS = RMSE_meta_LS;
params.a_hat_LS = a_hat_LS;
params.b_hat_LS = b_hat_LS;

%save('Data/params','params')

%disp(omega_sol);
%disp(params.w);
%disp(RMSE)
end
