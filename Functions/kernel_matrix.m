function K = kernel_matrix(X,eta,symb,Y)
%KERNEL_MATRIX 
%   Kernel matrix defined with respect to ARD lengthscales eta and for all
%   input locations X

%  X matrix NxD

import casadi.*

switch nargin
    
    case 3
    if symb
        K = SX.sym('K',size(X,1),size(X,1));
    else
        K = zeros(size(X,1),size(X,1));
    end
    for i=1:size(X,1)
        for j=i:size(X,1)
            K(i,j) = kernel_scalar(X(i,:)',X(j,:)',eta);
            K(j,i) = K(i,j);
        end
    end

    case 4
    if symb
        K = SX.sym('K',size(X,1),size(Y,1));
    else
        K = zeros(size(X,1),size(Y,1));
    end
    for i=1:size(X,1)
        for j=1:size(Y,1)
            K(i,j) = kernel_scalar(X(i,:)',Y(j,:)',eta);
        end
    end


end

