function params = MCMC(params,X,y,doplot)

M = params.M;
N = params.N;
omegas_true = params.w; % it is a E/2 x d matrix
E = params.E;
d = params.D;

alfas_true = params.alfa;

G = 50; %number of chains

Y = zeros(N,M);
for m=1:M
    Y(:,m) = y{m};
end

[lambdas_est, sigmas2_est, omegas_est, Phi_est] = optiMargLikGibbs(M,N,E,X,Y,G);

params.omegas_MCMC = omegas_est;

% refinement step

all_lambdas = cell(M,1);
all_sigmas2 = cell(M,1);
alfas_est = zeros(E,M);

for m = 1:M
    [lambda_est, sigma2_est, alfa_est] = optiMargLikGibbs_ref(N,E,X,Y(:,m),G,omegas_est);
    all_lambdas{m} = lambdas_est;
    all_sigmas2{m} = sigma2_est;
    alfas_est(:,m) = alfa_est; 
end

params.lambdas_MCMC = all_lambdas;
params.sigmas2_MCMC = all_sigmas2;
params.alfas_MCMC = alfas_est;
params.a_MCMC = alfas_est(1:params.E/2,:);
params.b_MCMC = alfas_est(params.E/2 + 1:end,:);

% RMSE

X_test = [];
for i=1:params.D
    X_test = [X_test,linspace(params.t_minmax(1),params.t_minmax(2),1000)'];
end

Y_true = zeros(size(X_test,1),M);

RMSEs = zeros(params.M,1);
for m=1:M
    Y_true(:,m) = generatePhi(omegas_true,X_test)*alfas_true(:,m);
    Y_est(:,m) = generatePhi(omegas_est,X_test)*alfas_est(:,m);
    RMSEs(m) = sqrt(mean((Y_true(:,m) - Y_est(:,m)).^2));
end



if doplot
    for i=1:params.M
        figure;
        hold on
        plot(Y_est(:,i));
        %plot(y{i},'*');
        plot(Y_true(:,i));
    end
end

params.RMSE_MCMC = RMSEs;