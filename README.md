## Description
Multi-task learning routines examined in paper [1]. 

The MATLAB code relies entrirely on [CasADi](https://web.casadi.org/) and [IPOPT](https://coin-or.github.io/Ipopt/) 

Run `main.m` to execute the simulations of the methods described in the paper.


## Authors and acknowledgment
[1] E. Arcari*, A. Scampicchio*, A. Carron, M. N. Zeilinger, “Bayesian Multi-Task Learning
using Finite-Dimensional Models: A Comparative Study”, IEEE Conference on Decision
and Control 2021
